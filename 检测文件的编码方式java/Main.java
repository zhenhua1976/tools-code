package javatest;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by liuzhenhua on 2015/3/26 0026.
 */
public class Main {
    public static void main(String[] args){
        if (args.length != 2){
            System.out.println("该工具是用来检测某个目录下的所有文件编码方式是否都是某种特定的编码方式");
            System.out.println("请输入两个参数，依次是文件路径、编码方式");
            System.exit(0);
        }

        String rootUrl = args[0];
        String defaultEncoding = args[1];
//        String rootUrl = "E:/android/workspace/Location/src";
        File rootDir = new File(rootUrl);
        findFiles(rootDir, defaultEncoding);
    }

    public static void findFiles(File file, String defaultEncoding){
        for (File subFile : file.listFiles()){
            if (subFile.isDirectory()){
                findFiles(subFile, defaultEncoding);
            }else {
                try {
                    FileReader reader = new FileReader(subFile);
                    String encoding= reader.getEncoding();
                    System.out.println(encoding);
                    reader.close();
                    if (!encoding.equals(defaultEncoding)){
                        System.out.println(subFile.getName()+"的编码方式为："+encoding);
                    }
                } catch (IOException e) {
                    System.out.println("获取文件"+subFile.getName()+"的编码方式时出错");
                    e.printStackTrace();
                }

            }
        }
    }

}
